function categorizeRecipe(recipe) {
    if (recipe.needsOven && recipe.needsSpecializedTool && recipe.needsExoticFood) {
        return "difficult"
    }
    else if (recipe.needsOven | recipe.needsSpecializedTool | recipe.needsExoticFood){
            return "middle difficulty"
    }
    return "easy"
}

export { categorizeRecipe }