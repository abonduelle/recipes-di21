import { expect } from 'chai'
import { categorizeRecipe } from '../../categorizeRecipe.js'

describe("a category", () => {
    it("must be tagged as difficult", () => {
        let recipe = {
            _id: "12344556",
            name: "couscous",
            ingredients: ["semoule", "pois chiches", "mergez"],
            needsOven: true,
            needsSpecializedTool: true,
            needsExoticFood: true,
            instructions:["faites du couscous, c'est bon"]
        }
        let category = categorizeRecipe(recipe)
        expect(category).to.equal("difficult")
    })
    it("mon 2eme test", () => {
        expect(1).to.equal(1)
    })
})
describe("b category", () => {
    it("must be tagged as middle difficulty", () => {
        let recipe = {
            _id: "12344557",
            name: "ommelette aux champignons",
            ingredients: ["oeuf", "champignons"],
            needsOven: false,
            needsSpecializedTool: true,
            needsExoticFood: false,
            instructions:["battre les oeufs au fouet"]
        }
        let category = categorizeRecipe(recipe)
        expect(category).to.equal("middle difficulty")
    })
    it("mon 2eme test", () => {
        expect(1).to.equal(1)
    })
})